import os
import pickle
import random
import cv2

import MFCCEngine
import operations_on_files as onf
from os.path import join
from numpy import array, reshape, resize
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC

class SVCEngine:
    def __init__(self, word: str):
        self.categories = ['correct', 'incorrect']
        self.path_to_audio = [join("C:\\Users\\Kasia\\Music\\ProjektIWD", word, "correct"),
                              join("C:\\Users\\Kasia\\Music\\ProjektIWD", word, "incorrect")]

        self.path_to_images = [join("C:\\Users\\Kasia\\Pictures\\ProjektIWD", word, "correct"),
                               join("C:\\Users\\Kasia\\Pictures\\ProjektIWD", word, "incorrect")]

        self.path_to_image_to_predict = join("C:\\Users\\Kasia\\Pictures\\ProjektIWD", word, "pending")
        self.path_to_audio_to_predict = join("C:\\Users\\Kasia\\Music\\ProjektIWD", word, "pending", "audio.wav")

        self.mfccEngine = MFCCEngine.MFCCEngine()

        for counter, path_to_audio_file in enumerate(self.path_to_audio):
            list_of_audios = onf.get_list_of_wave_file(path_to_audio_file)
            for counter_sub, wav_file in enumerate(list_of_audios):
                mfcc_matrix = self.mfccEngine.compute_mfcc(wav_file)
                norm_mfcc_matrix = self.mfccEngine.normalize_mfcc(mfcc_matrix)
                self.mfccEngine.create_image(norm_mfcc_matrix, self.path_to_images[counter], str(counter_sub) + '.bmp')

        self.create_prediction_image()

        self.prepare_training_set(word)
        self.model = self.train_model(word)

    def prepare_training_set(self, word: str):
        data_train = []

        categories = ['correct', 'incorrect']

        for counter_folder, folder in enumerate(self.path_to_images):
            for counter_img, img in enumerate(os.listdir(folder)):
                img_path = join(folder, img)
                sample = cv2.imread(img_path, 0)
                image = array(sample).flatten()
                data_train.append([image, categories[counter_folder]])

        path_to_data_training = join("D:\\recordandclassifier\\resources", word + ".pickle")
        pick_in = open(path_to_data_training, 'wb')
        pickle.dump(data_train, pick_in)
        pick_in.close()

    def train_model(self, word: str):
        pick_in = open(join("D:\\recordandclassifier\\resources", word + ".pickle"), 'rb')
        train_data = pickle.load(pick_in)
        pick_in.close()

        random.shuffle(train_data)
        features = []
        labels = []

        for feature, label in train_data:
            features.append(feature)
            labels.append(label)

        xtrain, xtest, ytrain, ytest = train_test_split(features, labels, test_size=0.25)

        model = SVC(C=5, kernel='linear', gamma='auto', probability=True)
        model.fit(xtrain, ytrain)

        return model

    def create_prediction_image(self):
        wav_file = self.path_to_audio_to_predict
        mfcc_matrix = self.mfccEngine.compute_mfcc(wav_file)
        norm_mfcc_matrix = self.mfccEngine.normalize_mfcc(mfcc_matrix)
        self.mfccEngine.create_image(norm_mfcc_matrix, self.path_to_image_to_predict, str(0) + ".bmp")

    def predicting_correctness(self):
        path = join(self.path_to_image_to_predict, "0.bmp")

        sample = cv2.imread(path, 0)
        img_size = resize(sample, (26, 190))
        image = [img_size.flatten()]

        result = self.model.predict(image)[0]
        print("Predicted audio is: " + result)

