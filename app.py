from flask import Flask
from flask import render_template
from flask import request
from os.path import join
from SVCEngine import SVCEngine
import operations_on_files as onf

app = Flask(__name__)

@app.route("/", methods=['POST', 'GET'])
def index():
    if request.method == "POST":
        word = str(request.args.get('word'))

        f = request.files['audio_data']
        path_to_save = join("C:\\Users\\Kasia\\Music\\ProjektIWD", word, "pending")

        with open('audio.wav', 'wb') as audio:
            path = join(path_to_save, audio.name)
            f.save(path)
        print('file uploaded successfully')

        svc_machine = SVCEngine(word)
        result = svc_machine.predicting_correctness()

        if result == str('correct'):
            onf.move_to_correct_audio(path, word)
        else:
            onf.move_to_incorrect_audio(path, word)

        return render_template('index.html', request="POST")
    else:
        return render_template("index.html")


if __name__ == "__main__":
    app.run(debug=True)