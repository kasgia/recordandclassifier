import unittest
import MFCCEngine
from numpy import logical_and, array

class MyTestCase(unittest.TestCase):
    mfcc_engine = MFCCEngine.MFCCEngine()

    def test_compute_mfcc_not_wave_file(self):
        self.assertRaises(EOFError, self.mfcc_engine.compute_MFCC, 'C:\\Users\\Kasia\\Music\\ProjektIWD\\Inny\\another_file.txt')

    def test_compute_mfcc_WAV_file(self):
        self.assertNotEqual(self.mfcc_engine.compute_mfcc('C:\\Users\\Kasia\\Music\\ProjektIWD\\Ader\\01.wav'), [])

    def test_normalize_mfcc(self):
        matrix = self.mfcc_engine.compute_mfcc('C:\\Users\\Kasia\\Music\\ProjektIWD\\Ader\\01.wav')
        normal_matrix = self.mfcc_engine.normalize_mfcc(matrix)
        self.mfcc_engine.create_image(normal_matrix, 'C:\\Users\\Kasia\\Music\\ProjektIWD\\Inny', 'to_analyse.bmp')
        self.assertTrue(logical_and(normal_matrix >= 0, normal_matrix <= 255).all())

if __name__ == '__main__':
    unittest.main()
