import unittest
import operations_on_files as onf

class MyTestCase(unittest.TestCase):
    def test_list_wave_file_empty_folder(self):
        self.assertListEqual([], onf.get_list_of_wave_file("C:\\Users\\Kasia\\Music\\ProjektIWD\Inny"))

    def test_list_wave_file_folder_with_txt_file(self):
        self.assertListEqual(['C:\\Users\\Kasia\\Music\\ProjektIWD\Oder\\01.wav',
                              'C:\\Users\\Kasia\\Music\\ProjektIWD\\Oder\\02.wav'],
                             onf.get_list_of_wave_file('C:\\Users\\Kasia\\Music\\ProjektIWD\\Oder'))

    def test_list_wave_file_folder_only_wav_files(self):
        self.assertListEqual(['C:\\Users\\Kasia\\Music\\ProjektIWD\Ader\\01.wav',
                              'C:\\Users\\Kasia\\Music\\ProjektIWD\\Ader\\02.wav'],
                             onf.get_list_of_wave_file('C:\\Users\\Kasia\\Music\\ProjektIWD\\Ader'))

    def test_open_WAV_file(self):
        self.assertNotEqual(onf.open_wav_file('C:\\Users\\Kasia\\Music\\ProjektIWD\Ader\\01.wav'), [])

if __name__ == '__main__':
    unittest.main()
