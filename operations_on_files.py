from os import listdir
from os.path import isfile, join
from numpy import array
from scipy.io.wavfile import read
import shutil
from os import rename

def get_list_of_wave_file(path: str) -> list:
    """
    Args:
        path (str): path to folder with file

    Returns:
        List of paths to WAV files. If folder doesn't contain WAV files, function returns empty list
    """
    return [join(path, f) for f in listdir(path) if isfile(join(path, f)) and f.split('.')[1] in 'wav']

def open_wav_file(path: str):
    rate, file = read(path)
    return array(file[:, 1], dtype=float)

def move_files(org_path: str, target_path: str):
    shutil.move(org_path, target_path)

def set_last_number_of_file(path: str):
    return len(listdir(path))

def move_to_correct_audio(path: str, word: str):
    path_to_audio = join("C:\\Users\\Kasia\\Music\\ProjektIWD", word, "correct")
    no_of_sample = set_last_number_of_file(join("C:\\Users\\Kasia\\Music\\ProjektIWD", word, "correct"))+1
    new_path_to_audio = rename(path, join("C:\\Users\\Kasia\\Music\\ProjektIWD", word, "correct", str(no_of_sample) + ".wav"))

def move_to_incorrect_audio(path: str, word: str):
    path_to_audio = join("C:\\Users\\Kasia\\Music\\ProjektIWD", word, "incorrect")
    no_of_sample = set_last_number_of_file(join("C:\\Users\\Kasia\\Music\\ProjektIWD", word, "incorrect"))+1
    new_path_to_audio = rename(path, join("C:\\Users\\Kasia\\Music\\ProjektIWD", word, "incorrect", str(no_of_sample) + ".wav"))
